*** Settings ***
Documentation       Test Case for the AMH and Message Manager Integration
...  \n             Test Case 1 : AMH Login and Logout With Valid User
...  \n             Test Case 2 : AMH Sending Message to AMH Manual FLow
...  \n             Test Case 3 : AMH Sending Message to SAA Akura Manual FLow

Resource          ../Resources/Page_Objects/Login/Login.robot
Resource          ../Resources/Page_Objects/Operations/Operations.robot
Resource          ../../Web_UI/TestsCases/resource.robot

Library           RequestsLibrary
*** Variables ***
${TEMPLATE_NAME}    MT_199_AMH_TO_SAA_AKURA_SU3_JJJ

*** Test Cases ***
TC001-Health With Status Code from Servers
    [Documentation]     Test Case Steps :
    ...   \n Step 1    Open the Browser and Navigate to AMH Web Portal url : ${LOGIN_URL_AMH}
    ...   \n Step 2    Validate the Home page is opened Properly with Dashboard Content.
    ...   \n Step 3    Logout from the AMH web protal and close all browsers.
    [Tags]      AMH_HEALTH
    create session   ss   https://172.31.31.147:8443
    ${resp}   get on session    ss   /amhweb/health-check
    log  Status Code from the URL [https://172.31.31.147:8443/amhweb/health-check] GET by API is ${resp.status_code}   console=true
    log  Content of the URL: "https://172.31.31.147:8443/amhweb/health-check" for AMH and Status is of the Service : ${resp.content}   console=true
    should be equal as strings   200   ${resp.status_code}
    should contain   ${resp.text}   HTTP status: 200 OK<BR/>
    should contain   ${resp.text}   <H1>AMH Health Check</H1>

    create session   ss   https://172.31.31.124:8443
    ${resp}   get on session    ss   /amhweb/health-check
    log  Status Code from the URL [https://172.31.31.124:8443/amhweb/health-check] GET by API is ${resp.status_code}   console=true
    log  Content of the URL: "https://172.31.31.124:8443/amhweb/health-check" for AMH and Status is of the Service : ${resp.content}   console=true
    should be equal as strings   200   ${resp.status_code}
    should contain   ${resp.text}   HTTP status: 200 OK<BR/>
    should contain   ${resp.text}   <H1>AMH Health Check</H1>

TC002-Health With Operations Checks
    [Documentation]     Test Case Steps :
    ...   \n Step 1    Open the Browser and Navigate to AMH Web Portal url : ${LOGIN_URL_AMH}
    ...   \n Step 2    Validate the Home page is opened Properly with Dashboard Content.
    ...   \n Step 3    Logout from the AMH web protal and close all browsers.
    [Tags]      AMH_HEALTH
    Log     ${PREV TEST STATUS}
    IF    '${PREV TEST STATUS}' == 'PASS'
        Log   Server is up and running we are good with UI Health Check    console=true
    ELSE
        fail  Server is not up and running, Please troubleshoot   console=true
    END
    Login.Open Browser To Login Page
    Login to the AMH Web Application
    Navigate to the Operation Section Services With By Business Group
    Validate the Section for Status in Rows     Swift-FIN-LT    7    Started
    Validate the Section for Status in Rows     SAG-Cluster    7    Started
    Logout from the AMH Web Application
    sleep   5s
    [Teardown]    run keywords      Close Browser