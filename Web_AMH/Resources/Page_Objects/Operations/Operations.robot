*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String
Library           Collections
*** Variables ***

*** Keywords ***
Navigate to the Operation Section Services With ${Section}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   mouse over       //button[text()='Operations']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   Click Element       //button[text()='Operations']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   mouse over       //span[text()='${Section}']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   Click Element       //span[text()='${Section}']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   page should contain   AMH-Local

Validate the Section for Status in Rows
    [Arguments]     ${section}=Swift-FIN-LT    ${rows}=7    ${Status}=Started
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}    scroll element into view         //*[contains(text(),"<${section}>")]
    ${table}   create list   ${EMPTY}
    FOR  ${i}  IN RANGE   1   ${rows}
        ${row}   scrap the row for each column   ${section}   ${i}
        #log   ${row}    console=true
        ${ssts}   run keyword and return status    should contain    ${row}   ${Status}
        run keyword if   ${ssts}   append to list    ${table}    ${row}
    END
    log   ${table}
    should not be empty    ${table}
    FOR   ${row}   IN  @{table}
        Log   Some LT node are in started Status as below   console=true
        log   ${row}   console=true
    END

scrap the row for each column
    [Arguments]     ${section}    ${num}
    ${countTd}   get webelements   //*[contains(text(),"<${section}>")]/ancestor::td[2]/parent::tr/following-sibling::tr[${num}]/td
    ${Data}   set variable   Row ${Num}=
    FOR   ${td}  IN  @{countTd}
        ${txt}   get text   ${td}
        ${Data}  set variable   ${Data} | ${txt}
    END
    [Return]    ${Data}